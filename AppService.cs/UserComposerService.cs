﻿using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.IAppComposerService;
using Falley.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppcomposerService
{
    public class UserComposerService : IUserComposerService
    {

        private readonly IExperienceService _experienceService;
        private readonly IDutyService _dutyService;
        //private readonly IPhotoService _photoService;
        private readonly IProjectService _projectService;
        private readonly IResumeService _resumeService;
        private readonly IEducationService _educationService;
        private readonly ISkillService _skillService;
        private readonly IUserService _userService;

        public UserComposerService(
            IExperienceService experienceService,
            IDutyService dutyService,
            //IPhotoService photoService,
            IProjectService projectService,
            IResumeService resumeService,
            IEducationService educationService,
            ISkillService skillService,
            IUserService userService)
        {
            _experienceService = experienceService;
            _dutyService = dutyService;
            //_photoService = photoService;
            _projectService = projectService;
            _resumeService = resumeService;
            _educationService = educationService;
            _skillService = skillService;
            _userService = userService;
        }

        public async Task<UserViewModel> GetUserProfileContact(Guid userId)
        {
            var userViewModel = new UserViewModel();

            var userProfile = await _userService.GetUserProfileAsync(userId);
            //var photoProfile = await _photoService.GetPhotoProfileAsync(userId);
            var resumeProfile = await _resumeService.GetResumeProfileAsync(userId);
            var skillProfile = await _skillService.GetSkillProfilesAsync(userId);
            var educationProfile = await _educationService.GetEducationProfilesAsync(userId);
            var projectProfile = await _projectService.GetProjectProfiles(userId);
            var experienceProfiles = await _experienceService.GetExperienceProfilesAsync(userId);

            if (!experienceProfiles.IsNullOrEmpty())
            {
                foreach (var experience in experienceProfiles)
                {
                    var dutyProfile = await _dutyService.GetDutyProfilesAsync(experience.Id);
                    userViewModel.ExperienceDutiesViewModel = new List<ExperienceDutyViewModel>
                    {
                        new ExperienceDutyViewModel
                        {
                            ExperienceProfile = experience,
                            DutyProfiles = dutyProfile
                        }

                    };

                }

            }

            userViewModel.ProjectProfiles = projectProfile;
            userViewModel.Resume = resumeProfile;
            userViewModel.SkillProfiles = skillProfile;
            userViewModel.EducationProfiles = educationProfile;

            return userViewModel;
        }
    }
}
