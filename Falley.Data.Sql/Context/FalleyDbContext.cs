﻿using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Data.Sql.Context
{
    public class FalleyDbContext : DbContext
    {
        public FalleyDbContext(DbContextOptions<FalleyDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Education>().ToTable("Educations");
            modelBuilder.Entity<Resume>().ToTable("Resumes");
            modelBuilder.Entity<Photo>().ToTable("Photos");
            modelBuilder.Entity<Experience>().ToTable("Experiences");
            modelBuilder.Entity<Duty>().ToTable("Duties");
            modelBuilder.Entity<Skill>().ToTable("Skills");
            modelBuilder.Entity<Project>().ToTable("Projects");
        }

        internal DbSet<User> Users { get; set; }
        internal DbSet<Education> Educations { get; set; }
        internal DbSet<Resume> Resumes { get; set; }
        internal DbSet<Photo> Photos { get; set; }
        internal DbSet<Experience> Experiences { get; set; }
        internal DbSet<Duty> Duties { get; set; }
        internal DbSet<Skill> Skills { get; set; }
        internal DbSet<Project> Projects { get; set; }
    }
}
