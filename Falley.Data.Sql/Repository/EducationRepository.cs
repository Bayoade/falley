﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class EducationRepository : IEducationRepository
    {
        private readonly FalleyDbContext _dbContext;
        public EducationRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;

        }
        public Task CreateEducationAsync(EducationProfile profile, Guid userId)
        {
            var education = Mapper.Map<Education>(profile);
            education.UserId = userId;

            _dbContext.Educations.Add(education);
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteEducationAsync(Guid id)
        {
            var education = new Education
            {
                Id = id
            };

            _dbContext.Educations.Attach(education);
            _dbContext.Educations.Remove(education);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<EducationProfile> GetEducationProfileAsync(Guid educationId)
        {
            var education = await _dbContext.Educations.FirstOrDefaultAsync(x => x.Id == educationId);
            return Mapper.Map<EducationProfile>(education);
        }

        public async Task<IList<EducationProfile>> GetEducationProfilesAsync(Guid userId)
        {
            var educations = await _dbContext.Educations.Where(x => x.UserId == userId).ToListAsync();

            return Mapper.Map<List<EducationProfile>>(educations);
        }

        public async Task UpdateEducationAsync(EducationProfile profile, Guid userId)
        {
            var educationDb = await _dbContext.Educations.FirstOrDefaultAsync(x => x.Id == profile.Id);

            if(educationDb.UserId == userId)
            {
                educationDb.Period = profile.Period;
                educationDb.Degree = profile.Degree;
                educationDb.School = profile.School;

                _dbContext.Educations.Update(educationDb);

                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
