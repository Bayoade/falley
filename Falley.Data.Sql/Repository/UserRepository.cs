﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly FalleyDbContext _dbContext;

        public UserRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task CreateUserAsync(UserProfile userProfile)
        {
            _dbContext.Users.Add(Mapper.Map<User>(userProfile));

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteUserAsync(params Guid[] ids)
        {
            var users = new List<User>();

            foreach (var id in ids)
            {
                var user = new User
                {
                    Id = id,
                };

                users.Add(user);
            }

            _dbContext.Users.AttachRange(users);
            _dbContext.Users.RemoveRange(users);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IList<UserProfile>> GetAllUserProfileAsync()
        {
            var userProfiles = await _dbContext.Users.ToListAsync();
            return Mapper.Map<List<UserProfile>>(userProfiles);
        }

        public async Task<UserProfile> GetUserProfileAsync(Guid id)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            return Mapper.Map<UserProfile>(user);
        }

        public async Task<UserProfile> GetUserProfileByEmailAsync(string email)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Email == email);
            return Mapper.Map<UserProfile>(user);
        }

        public async Task UpdateUserLoginAsync(UserProfile update)
        {
            var userDb = await GetUserAsync(update.Id);

            userDb.Password = update.Password;
            userDb.Salt = update.Salt;

            _dbContext.Users.Update(userDb);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateUserDetailsAsync(UserProfile update)
        {
            var userDb = await GetUserAsync(update.Id);

            userDb.LastName = update.LastName;
            userDb.MiddleName = update.MiddleName;
            userDb.FirstName = update.FirstName;
            userDb.Email = update.Email;

            _dbContext.Users.Update(userDb);

            await _dbContext.SaveChangesAsync();
        }

        private Task<User> GetUserAsync(Guid id)
        {
            return _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
