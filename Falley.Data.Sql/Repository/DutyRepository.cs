﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class DutyRepository : IDutyRepository
    {
        private readonly FalleyDbContext _dbContext;
        public DutyRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task CreateDutyAsync(DutyProfile profile, Guid experienceId)
        {
            var duty = Mapper.Map<Duty>(profile);
            duty.ExperienceId = experienceId;

            _dbContext.Duties.Add(duty);
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteDutyAsync(params Guid[] ids)
        {
            var duties = new List<Duty>();
            foreach (var id in ids)
            {
                var duty = new Duty
                {
                    Id = id
                };

                duties.Add(duty);
            }
            _dbContext.Duties.AttachRange(duties);
            _dbContext.Duties.RemoveRange(duties);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<DutyProfile>> GetDutyProfilesAsync(Guid experienceId)
        {
            var duties = await _dbContext.Duties.Where(x => x.ExperienceId == experienceId).ToListAsync();

            return Mapper.Map<List<DutyProfile>>(duties);
        }

        public Task UpdateDutyAsync(DutyProfile profile)
        {
            var duty = Mapper.Map<Duty>(profile);

            _dbContext.Duties.Attach(duty);
            _dbContext.Duties.Update(duty);

            return _dbContext.SaveChangesAsync();
        }
    }
}
