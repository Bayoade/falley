﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class ResumeRepository : IResumeRepository
    {
        private readonly FalleyDbContext _dbContext;
        public ResumeRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task CreateResumeAsync(ResumeProfile profile, Guid userId)
        {
            var resume = Mapper.Map<Resume>(profile);
            resume.UserId = userId;

            _dbContext.Resumes.Add(resume);

            return _dbContext.SaveChangesAsync();
        }

        public async Task DeleteResumeAsync(params Guid[] ids)
        {
            var resumes = new List<Resume>();
            foreach (var userId in ids)
            {
                var resume = await _dbContext.Resumes.FirstOrDefaultAsync(x => x.UserId == userId);

                resumes.Add(resume);
            }
            _dbContext.Resumes.RemoveRange(resumes);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<ResumeProfile> GetResumeProfileAsync(Guid userId)
        {
            var resume = await _dbContext.Resumes.FirstOrDefaultAsync(x => x.UserId == userId);
            return Mapper.Map<ResumeProfile>(resume);
        }

        public async Task UpdateResumeAsync(ResumeProfile profile, Guid userId)
        {
            var resume = Mapper.Map<Resume>(profile);
            var resumeDb = await _dbContext.Resumes.FirstOrDefaultAsync(x => x.Id == profile.Id );

            //Mapper.Map(resume, resumeDb);
            if(resumeDb.UserId == userId)
            {
                resumeDb.Description = resume.Description;
                resumeDb.Email = resume.Email;
                resumeDb.Address = resume.Address;
                resumeDb.PhoneNumber = resume.PhoneNumber;
                resumeDb.Position = resume.Position;

                _dbContext.Resumes.Update(resumeDb);

                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
