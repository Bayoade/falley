﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class PhotoRepository : IPhotoRepository
    {
        private readonly FalleyDbContext _dbContext;
        public PhotoRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task CreatePhotoAsync(PhotoProfile profile, Guid userId)
        {
            var photo = Mapper.Map<Photo>(profile);

            photo.UserId = userId;
            
            _dbContext.Photos.Add(photo);

            return _dbContext.SaveChangesAsync();
        }

        public async Task DeletePhotoAsync(Guid userId)
        {
            var photo = await _dbContext.Photos.FirstOrDefaultAsync(x => x.UserId == userId);

            _dbContext.Photos.Remove(photo);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<PhotoProfile> GetPhotoProfileAsync(Guid userId)
        {
            var photo = await _dbContext.Photos.FirstOrDefaultAsync(x => x.UserId == userId);

            return Mapper.Map<PhotoProfile>(photo);
        }
    }
}
