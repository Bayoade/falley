﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class ExperienceRepository : IExperienceRepository
    {
        private readonly FalleyDbContext _dbContext;
        public ExperienceRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task CreateExperienceAsync(ExperienceProfile profile, Guid userId)
        {
            var experience = Mapper.Map<Experience>(profile);

            experience.UserId = userId;
            _dbContext.Experiences.Add(experience);
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteExperienceAsync(params Guid[] ids)
        {
            var experiences = new List<Experience>();
            foreach (var id in ids)
            {
                var experience = new Experience
                {
                    Id = id
                };
                experiences.Add(experience);
            }

            _dbContext.Experiences.AttachRange(experiences);
            _dbContext.Experiences.RemoveRange(experiences);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<ExperienceProfile> GetExperienceProfileAsync(Guid id)
        {
            var experience = await _dbContext.Experiences.FirstOrDefaultAsync(x => x.Id == id);

            return Mapper.Map<ExperienceProfile>(experience);
        }

        public async Task<IList<ExperienceProfile>> GetExperienceProfilesAsync(Guid userId)
        {
            var experiences = await _dbContext.Experiences.Where(x => x.UserId == userId).ToListAsync();
            return Mapper.Map<List<ExperienceProfile>>(experiences);
        }

        public async Task UpdateExperienceAsync(ExperienceProfile profile, Guid userId)
        {
            var experienceDb = await _dbContext.Experiences.FirstOrDefaultAsync(x => x.Id == profile.Id);
            if(experienceDb.UserId == userId)
            {
                experienceDb.Period = profile.Period;
                experienceDb.Position = profile.Position;
                experienceDb.Website = profile.Website;
                experienceDb.Company = profile.Company;

                _dbContext.Experiences.Update(experienceDb);

                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
