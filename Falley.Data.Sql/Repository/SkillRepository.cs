﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class SkillRepository : ISkillRepository
    {
        private readonly FalleyDbContext _dbContext;
        public SkillRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task CreateSkillProfileAsync(SkillProfile profile, Guid userId)
        {
            var skill = Mapper.Map<Skill>(profile);
            skill.UserId = userId;

            _dbContext.Skills.Add(skill);

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteSkillProfileAsync(params Guid[] ids)
        {
            var skills = new List<Skill>();
            foreach (var id in ids)
            {
                var skill = new Skill
                {
                    Id = id
                };

                skills.Add(skill);
            }

            _dbContext.Skills.AttachRange(skills);
            _dbContext.Skills.RemoveRange(skills);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<SkillProfile>> GetSkillProfilesAsync(Guid userId)
        {
            var skills = await _dbContext.Skills.Where(x => x.UserId == userId).ToListAsync();

            return Mapper.Map<List<SkillProfile>>(skills);
        }
    }
}
