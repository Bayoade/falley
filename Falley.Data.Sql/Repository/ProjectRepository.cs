﻿using AutoMapper;
using Falley.Core.IRepository;
using Falley.Core.Model;
using Falley.Data.Sql.Context;
using Falley.Data.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Data.Sql.Repository
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly FalleyDbContext _dbContext;
        public ProjectRepository(FalleyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task CreateProjectAsync(ProjectProfile profile, Guid userId)
        {
            var project = Mapper.Map<Project>(profile);
            project.UserId = userId;
            _dbContext.Projects.Add(project);

            return _dbContext.SaveChangesAsync();
        }

        public async Task DeleteProjectAsync(params Guid[] ids)
        {
            var projects = new List<Project>();
            foreach (var id in ids)
            {
                var project = await _dbContext.Projects.FirstOrDefaultAsync(x => x.Id == id);
                projects.Add(project);
            }
            _dbContext.Projects.RemoveRange(projects);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<ProjectProfile> GetProjectProfile(Guid id)
        {
            var project = await _dbContext.Projects.FirstOrDefaultAsync(x => x.Id == id);
            return Mapper.Map<ProjectProfile>(project);
        }

        public async Task<IList<ProjectProfile>> GetProjectProfiles(Guid userId)
        {
            var projects = await _dbContext.Projects.Where(x => x.UserId == userId).ToListAsync();
            return Mapper.Map<IList<ProjectProfile>>(projects);
        }

        public async Task UpdateProjectAsync(ProjectProfile profile, Guid userId)
        {
            var projectDb = await _dbContext.Projects.FirstOrDefaultAsync(x => x.Id == profile.Id);

            if(projectDb.UserId == userId)
            {
                projectDb.ProjectName = profile.ProjectName;
                profile.ProjectRepository = profile.ProjectRepository;

                await _dbContext.SaveChangesAsync();
            }
            
        }
    }
}
