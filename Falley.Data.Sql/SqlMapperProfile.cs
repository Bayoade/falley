﻿using AutoMapper;
using Falley.Core.Model;
using Falley.Data.Sql.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Data.Sql
{
    public class SqlMapperProfile : Profile
    {
        public SqlMapperProfile()
        {
            CreateMap<UserProfile, User>()
                .ReverseMap()
                .ForMember(x => x.Name, y => y.Ignore());

            CreateMap<PhotoProfile, Photo>()
                .ForMember(x => x.UserId, y => y.Ignore());

            CreateMap<DutyProfile, Duty>()
                .ForMember(x => x.ExperienceId, y => y.Ignore());

            CreateMap<ExperienceProfile, Experience>()
                .ForMember(x => x.UserId, y => y.Ignore());

            CreateMap<ResumeProfile, Resume>()
                .ForMember(x => x.UserId, y => y.Ignore());

            CreateMap<EducationProfile, Education>()
                .ForMember(x => x.UserId, y => y.Ignore());

            CreateMap<SkillProfile, Skill>()
                .ForMember(x => x.UserId, y => y.Ignore());

            CreateMap<ProjectProfile, Project>()
               .ForMember(x => x.UserId, y => y.Ignore());
        }
    }
}
