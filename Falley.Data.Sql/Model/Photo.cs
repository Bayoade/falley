﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Data.Sql.Model
{
    internal class Photo
    {
        public Guid Id { get; set; }

        public string FileName { get; set; }

        public byte[] FileData { get; set; }

        public Guid UserId { get; set; }
    }
}
