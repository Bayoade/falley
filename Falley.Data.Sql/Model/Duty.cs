﻿using System;

namespace Falley.Data.Sql.Model
{
    internal class Duty
    {
        public Guid Id { get; set; }

        public string Workdone { get; set; }

        public Guid ExperienceId { get; set; }
    }
}
