﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Data.Sql.Model
{
    internal class Resume
    {
        public Guid Id { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Position { get; set; }

        public string Email { get; set; }

        public string Description { get; set; }

        public Guid UserId { get; set; }
    }
}
