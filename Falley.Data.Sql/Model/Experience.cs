﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Data.Sql.Model
{
    internal class Experience
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string Company { get; set; }

        public string Position { get; set; }

        public string Period { get; set; }

        public string Website { get; set; }
    }
}
