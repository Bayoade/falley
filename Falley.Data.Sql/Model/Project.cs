﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Data.Sql.Model
{
    internal class Project
    {
        public Guid Id { get; set; }

        public string ProjectRepository { get; set; }

        public string ProjectName { get; set; }

        public Guid UserId { get; set; }
    }
}
