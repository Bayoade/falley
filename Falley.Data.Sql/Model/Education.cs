﻿using System;

namespace Falley.Data.Sql.Model
{
    internal class Education
    {
        public Guid Id { get; set; }

        public string School { get; set; }

        public string Degree { get; set; }

        public string Period { get; set; }

        public Guid UserId { get; set; }
    }
}
