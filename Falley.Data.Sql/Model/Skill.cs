﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Data.Sql.Model
{
    internal class Skill
    {
        public Guid Id { get; set; }

        public string SkillName { get; set; }

        public Guid UserId { get; set; }
    }
}
