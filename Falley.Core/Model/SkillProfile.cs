﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Core.Model
{
    public class SkillProfile
    {
        public Guid Id { get; set; }

        public string SkillName { get; set; }
    }
}
