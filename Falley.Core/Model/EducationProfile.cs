﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Core.Model
{
    public class EducationProfile
    {
        public Guid Id { get; set; }

        public string School { get; set; }

        public string Degree { get; set; }

        public string Period { get; set; }
    }
}
