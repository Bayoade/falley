﻿using Falley.Core.Extensions;
using System;

namespace Falley.Core.Model
{
    public class UserProfile
    {
        private static string _name;

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Email { get; set; }

        //public bool IsAllowed { get; set; }

        //public string Password { get; set; }

        //public string Salt { get; set; }

        public string Name
        {
            get
            {
                _name = $"{FirstName} {LastName}";
                return _name;
            }
        }

        public string Password { get; set; }

        public string Salt { get; set; }
    }
}
