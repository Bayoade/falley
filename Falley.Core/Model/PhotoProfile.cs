﻿using System;

namespace Falley.Core.Model
{
    public class PhotoProfile
    {
        public Guid Id { get; set; }

        public string FileName { get; set; }

        public byte[] FileData { get; set; }
    }
}
