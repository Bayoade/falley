﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falley.Core.Model
{
    public class ProjectProfile
    {
        public Guid Id { get; set; }

        public string ProjectRepository { get; set; }

        public string ProjectName { get; set; }
    }
}
