﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Falley.Core.Extensions
{
    public class AppException : Exception
    {
        public AppException() : base() { }

        public AppException(string errorMessage) { }

        public AppException(string errorMessage, params object[] args) :
            base(String.Format(CultureInfo.CurrentCulture, errorMessage, args))
        {

        }
    }
}
