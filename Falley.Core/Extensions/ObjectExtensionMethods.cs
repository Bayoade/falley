﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Falley.Core.Extensions
{
    public static class ObjectExtensionMethods
    {

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || source.Count() == default(int);
        }

        public static bool IsNull<T>(this T source)
        {
            return source == null;
        }

        public static bool IsEmpty<T>(this T source) where T : struct
        {
            return source.Equals(default(T));
        }
    }
}
