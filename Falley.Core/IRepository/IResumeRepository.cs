﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IRepository
{
    public interface IResumeRepository
    {
        Task CreateResumeAsync(ResumeProfile profile, Guid userId);

        Task UpdateResumeAsync(ResumeProfile profile, Guid userId);

        Task DeleteResumeAsync(params Guid[] ids);

        Task<ResumeProfile> GetResumeProfileAsync(Guid userId);
    }
}
