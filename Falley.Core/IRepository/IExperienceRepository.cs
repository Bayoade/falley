﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IRepository
{
    public interface IExperienceRepository
    {
        Task CreateExperienceAsync(ExperienceProfile profile, Guid userId);

        Task UpdateExperienceAsync(ExperienceProfile profile, Guid userId);

        Task DeleteExperienceAsync(params Guid[] ids);

        Task<ExperienceProfile> GetExperienceProfileAsync(Guid id);

        Task<IList<ExperienceProfile>> GetExperienceProfilesAsync(Guid userId);
    }
}
