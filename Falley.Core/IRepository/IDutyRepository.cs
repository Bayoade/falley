﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IRepository
{
    public interface IDutyRepository
    {
        Task CreateDutyAsync(DutyProfile profile, Guid experienceId);

        Task UpdateDutyAsync(DutyProfile profile);

        Task DeleteDutyAsync(params Guid[] ids);

        Task<IList<DutyProfile>> GetDutyProfilesAsync(Guid experienceId);
    }
}
