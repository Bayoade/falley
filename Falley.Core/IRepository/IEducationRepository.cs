﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IRepository
{
    public interface IEducationRepository
    {
        Task CreateEducationAsync(EducationProfile profile, Guid userId);

        Task UpdateEducationAsync(EducationProfile profile, Guid userId);

        Task DeleteEducationAsync(Guid id);

        Task<IList<EducationProfile>> GetEducationProfilesAsync(Guid userId);

        Task<EducationProfile> GetEducationProfileAsync(Guid educationId);
    }
}
