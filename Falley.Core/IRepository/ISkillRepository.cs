﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IRepository
{
    public interface ISkillRepository
    {
        Task CreateSkillProfileAsync(SkillProfile profile, Guid userId);

        Task DeleteSkillProfileAsync(params Guid[] id);

        Task<IList<SkillProfile>> GetSkillProfilesAsync(Guid userId);
    }
}
