﻿using Falley.Core.Model;
using System;
using System.Threading.Tasks;

namespace Falley.Core.IRepository
{
    public interface IPhotoRepository
    {
        Task CreatePhotoAsync(PhotoProfile profile, Guid userId);

        Task DeletePhotoAsync(Guid userId);

        Task<PhotoProfile> GetPhotoProfileAsync(Guid userId);
    }
}
