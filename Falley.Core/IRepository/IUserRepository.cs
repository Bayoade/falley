﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IRepository
{
    public interface IUserRepository
    {
        Task CreateUserAsync(UserProfile user);

        Task DeleteUserAsync(params Guid[] ids);

        Task UpdateUserDetailsAsync(UserProfile update);

        Task UpdateUserLoginAsync(UserProfile update);

        Task<UserProfile> GetUserProfileAsync(Guid id);

        Task<UserProfile> GetUserProfileByEmailAsync(string email);

        Task<IList<UserProfile>> GetAllUserProfileAsync();
    }
}
