﻿using Falley.Core.Model;
using System;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IPhotoService
    {
        Task<Guid> CreatePhotoAsync(PhotoProfile profile, Guid userId);

        Task DeletePhotoAsync(Guid userId);

        Task<PhotoProfile> GetPhotoProfileAsync(Guid userId);
    }
}
