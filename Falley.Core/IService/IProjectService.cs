﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IProjectService
    {
        Task<Guid> CreateProjectAsync(ProjectProfile profile, Guid userId);

        Task DeleteProjectAsync(params Guid[] ids);

        Task UpdateProjectAsync(ProjectProfile profile, Guid userId);

        Task<IList<ProjectProfile>> GetProjectProfiles(Guid userId);

        Task<ProjectProfile> GetProjectProfile(Guid id);
    }
}
