﻿using Falley.Core.Model;
using System;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IResumeService
    {
        Task<Guid> CreateResumeAsync(ResumeProfile profile, Guid userId);

        Task UpdateResumeAsync(ResumeProfile profile, Guid userId);

        Task DeleteResumeAsync(Guid id);

        Task<ResumeProfile> GetResumeProfileAsync(Guid userId);
    }
}
