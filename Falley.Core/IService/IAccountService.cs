﻿using Falley.Core.Model;
using System;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IAccountService
    {
        Task<UserProfile> GetUserProfileByEmailAsync(string email);

        Task<UserProfile> AuthenticateAsync(string user, string password);

        Task UpdatePasswordLoginAsync(UserProfile profile);

        Task<Guid> CreateUserProfileAsync(UserProfile profile);

        Task UpdateUserProfileDetailsAsync(UserProfile profile);
    }
}
