﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IExperienceService
    {
        Task<Guid> CreateExperienceAsync(ExperienceProfile profile, Guid userId);

        Task UpdateExperienceAsync(ExperienceProfile profile, Guid userId);

        Task DeleteExperienceAsync(Guid id);

        Task<IList<ExperienceProfile>> GetExperienceProfilesAsync(Guid userId);

        Task<ExperienceProfile> GetExperienceProfileAsync(Guid id);
    }
}
