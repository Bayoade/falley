﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IDutyService
    {
        Task<Guid> CreateDutyAsync(DutyProfile profile, Guid experienceId);

        Task UpdateDutyAsync(DutyProfile profile);

        Task DeleteDutyAsync(Guid id);

        Task<IList<DutyProfile>> GetDutyProfilesAsync(Guid id);
    }
}
