﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IUserService
    {
        Task DeleteUserAsync(Guid id);
        
        Task<UserProfile> GetUserProfileAsync(Guid id);

        Task<IList<UserProfile>> GetAllUserProiles();

        Task<UserProfile> GetUserProfileByEmailAsync(string email);
    }
}
