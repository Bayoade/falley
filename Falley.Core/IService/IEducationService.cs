﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface IEducationService
    {
        Task<Guid> CreateEducationAsync(EducationProfile profile, Guid userId);

        Task UpdateEducationAsync(EducationProfile profile, Guid userId);

        Task DeleteEducationAsync(Guid id);

        Task<IList<EducationProfile>> GetEducationProfilesAsync(Guid userId);

        Task<EducationProfile> GetEducationProfileAsync(Guid educationId);
    }
}
