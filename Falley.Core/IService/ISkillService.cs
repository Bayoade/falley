﻿using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Core.IService
{
    public interface ISkillService
    {
        Task<Guid> CreateSkillProfileAsync(SkillProfile profile, Guid userId);

        Task DeleteSkillProfileAsync(params Guid[] ids);

        Task<IList<SkillProfile>> GetSkillProfilesAsync(Guid userId);
    }
}
