﻿using System;

namespace Falley.Core.IService
{
    public interface IUserContext
    {
        Guid UserId();

        bool IsAnonymousUser();

        string Name();
    }
}
