﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Service
{
    public class EducationService : IEducationService
    {
        private readonly IEducationRepository _educationRepository;
        public EducationService(IEducationRepository educationRepository)
        {
            _educationRepository = educationRepository;
        }

        public async Task<Guid> CreateEducationAsync(EducationProfile profile, Guid userId)
        {
            profile.Id = Guid.NewGuid();
            await _educationRepository.CreateEducationAsync(profile, userId);

            return profile.Id;
        }

        public Task DeleteEducationAsync(Guid id)
        {
            return _educationRepository.DeleteEducationAsync(id);
        }

        public Task<EducationProfile> GetEducationProfileAsync(Guid educationId)
        {
            return _educationRepository.GetEducationProfileAsync(educationId);
        }

        public Task<IList<EducationProfile>> GetEducationProfilesAsync(Guid userId)
        {
            return _educationRepository.GetEducationProfilesAsync(userId);
        }

        public Task UpdateEducationAsync(EducationProfile profile, Guid userId)
        {
            return _educationRepository.UpdateEducationAsync(profile, userId);
        }
    }
}
