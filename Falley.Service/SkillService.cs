﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Falley.Service
{
    public class SkillService : ISkillService
    {
        private readonly ISkillRepository _skillRepository;
        public SkillService(ISkillRepository skillRepository)
        {
            _skillRepository = skillRepository;
        }

        public async Task<Guid> CreateSkillProfileAsync(SkillProfile profile, Guid userId)
        {
            profile.Id = Guid.NewGuid();

            await _skillRepository.CreateSkillProfileAsync(profile, userId);

            return profile.Id;
        }

        public Task DeleteSkillProfileAsync(params Guid[] ids)
        {
            return _skillRepository.DeleteSkillProfileAsync(ids);
        }

        public Task<IList<SkillProfile>> GetSkillProfilesAsync(Guid userId)
        {
            return _skillRepository.GetSkillProfilesAsync(userId);
        }
    }
}
