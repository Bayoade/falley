﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Service
{
    public class ResumeService : IResumeService
    {
        private readonly IResumeRepository _resumeRepository;
        public ResumeService(IResumeRepository resumeRepository)
        {
            _resumeRepository = resumeRepository;
        }

        public async Task<Guid> CreateResumeAsync(ResumeProfile profile, Guid userId)
        {
            profile.Id = Guid.NewGuid();

            await _resumeRepository.CreateResumeAsync(profile, userId);

            return profile.Id;
        }

        public Task DeleteResumeAsync(Guid userId)
        {
            return _resumeRepository.DeleteResumeAsync(userId);
        }

        public Task<ResumeProfile> GetResumeProfileAsync(Guid userId)
        {
            return _resumeRepository.GetResumeProfileAsync(userId);
        }

        public Task UpdateResumeAsync(ResumeProfile profile, Guid userId)
        {
            return _resumeRepository.UpdateResumeAsync(profile, userId);
        }
    }
}
