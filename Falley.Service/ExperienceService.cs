﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Service
{
    public class ExperienceService : IExperienceService
    {
        private readonly IExperienceRepository _experienceRespository;
        public ExperienceService(IExperienceRepository experienceRespository)
        {
            _experienceRespository = experienceRespository;
        }

        public async Task<Guid> CreateExperienceAsync(ExperienceProfile profile, Guid userId)
        {
            profile.Id = Guid.NewGuid();

            await _experienceRespository.CreateExperienceAsync(profile, userId);

            return profile.Id;
        }

        public Task DeleteExperienceAsync(Guid experienceId)
        {
            return _experienceRespository.DeleteExperienceAsync(experienceId);
        }

        public Task<ExperienceProfile> GetExperienceProfileAsync(Guid id)
        {
            return _experienceRespository.GetExperienceProfileAsync(id);
        }

        public Task<IList<ExperienceProfile>> GetExperienceProfilesAsync(Guid userId)
        {
            return _experienceRespository.GetExperienceProfilesAsync(userId);
        }

        public Task UpdateExperienceAsync(ExperienceProfile profile, Guid userId)
        {
            return _experienceRespository.UpdateExperienceAsync(profile, userId);
        }
    }
}
