﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Service
{
    public class DutyService : IDutyService
    {
        private readonly IDutyRepository _dutyRepository;
        public DutyService(IDutyRepository dutyRepository)
        {
            _dutyRepository = dutyRepository;
        }

        public async Task<Guid> CreateDutyAsync(DutyProfile profile, Guid experienceId)
        {
            profile.Id = Guid.NewGuid();
            await _dutyRepository.CreateDutyAsync(profile, experienceId);

            return profile.Id;
        }

        public Task DeleteDutyAsync(Guid id)
        {
            return _dutyRepository.DeleteDutyAsync(id);
        }

        public Task<IList<DutyProfile>> GetDutyProfilesAsync(Guid experienceId)
        {
            return _dutyRepository.GetDutyProfilesAsync(experienceId);
        }

        public Task UpdateDutyAsync(DutyProfile profile)
        {
            return _dutyRepository.UpdateDutyAsync(profile);
        }
    }
}
