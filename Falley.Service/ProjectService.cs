﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Service
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task<Guid> CreateProjectAsync(ProjectProfile profile, Guid userId)
        {
            profile.Id = Guid.NewGuid();
            await _projectRepository.CreateProjectAsync(profile, userId);

            return profile.Id;
        }

        public Task DeleteProjectAsync(params Guid[] ids)
        {
            return _projectRepository.DeleteProjectAsync(ids);
        }

        public Task<ProjectProfile> GetProjectProfile(Guid id)
        {
            return _projectRepository.GetProjectProfile(id);
        }

        public Task<IList<ProjectProfile>> GetProjectProfiles(Guid userId)
        {
            return _projectRepository.GetProjectProfiles(userId);
        }

        public Task UpdateProjectAsync(ProjectProfile profile, Guid userId)
        {
            return _projectRepository.UpdateProjectAsync(profile, userId);
        }
    }
}
