﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Service
{
    public class PhotoService : IPhotoService
    {
        private readonly IPhotoRepository _photoRespository;
        public PhotoService(IPhotoRepository photoRespository)
        {
            _photoRespository = photoRespository;
        }

        public async Task<Guid> CreatePhotoAsync(PhotoProfile profile, Guid userId)
        {
            profile.Id = Guid.NewGuid();
            await _photoRespository.CreatePhotoAsync(profile, userId);

            return profile.Id;
        }

        public Task DeletePhotoAsync(Guid userId)
        {
            return _photoRespository.DeletePhotoAsync(userId);
        }

        public Task<PhotoProfile> GetPhotoProfileAsync(Guid userId)
        {
            return _photoRespository.GetPhotoProfileAsync(userId);
        }
    }
}
