﻿using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Falley.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Task DeleteUserAsync(Guid id)
        {
            return _userRepository.DeleteUserAsync(id);
        }

        public Task<IList<UserProfile>> GetAllUserProiles()
        {
            return _userRepository.GetAllUserProfileAsync();
        }

        public Task<UserProfile> GetUserProfileAsync(Guid userId)
        {
            return _userRepository.GetUserProfileAsync(userId);
        }

        public Task<UserProfile> GetUserProfileByEmailAsync(string email)
        {
            return _userRepository.GetUserProfileByEmailAsync(email);
        }
    }
}
