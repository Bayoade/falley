﻿using AutoMapper;
using Falley.Core.Extensions;
using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Core.Model;
using System;
using System.Threading.Tasks;
using static Falley.Core.Extensions.CrypoExtensions;

namespace Falley.Service
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;

        public AccountService(
            IUserRepository userRepository,
            IUserService userService)
        {
            _userRepository = userRepository;
            _userService = userService;
        }

        public async Task<UserProfile> AuthenticateAsync(string email, string password)
        {
            var userProfile = await _userRepository.GetUserProfileByEmailAsync(email);
            var passwordGenerated = password.ToSha256(userProfile.Salt, 72828);

            if (userProfile.Password != passwordGenerated)
            {
                return null;
            };

            //var userContext = GetUserContextUserId(userProfile.Id, _userService);

            return userProfile;
        }

        public async Task<Guid> CreateUserProfileAsync(UserProfile profile)
        {
            profile.Id = Guid.NewGuid();
            profile.Salt = GenerateSalt(32);

            profile.Password = profile.Password.ToSha256(profile.Salt, 72828);
            await _userRepository.CreateUserAsync(profile);

            return profile.Id;
        }

        public Task<UserProfile> GetUserProfileByEmailAsync(string email)
        {
            return _userRepository.GetUserProfileByEmailAsync(email);
        }

        public async Task UpdatePasswordLoginAsync(UserProfile profile)
        {
            profile.Salt = GenerateSalt(32);
            profile.Password = profile.Password.ToSha256(profile.Salt, 72828);

            await _userRepository.UpdateUserLoginAsync(profile);
        }

        public async Task UpdateUserProfileDetailsAsync(UserProfile profile)
        {
            await _userRepository.UpdateUserDetailsAsync(profile);
        }

        
    }
}
