﻿using Falley.Core.IService;
using Falley.Core.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using static Falley.Core.Extensions.ObjectExtensionMethods;

namespace Falley.Service
{
    public class UserContext : IUserContext
    {
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserContext(IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _userService = userService;
        }

        public Guid UserId()
        {
            var userProfile = GetUserProfile();
            if (userProfile.IsNull())
            {
                return Guid.Empty;
            }
            return userProfile.Id;
        }

        public bool IsAnonymousUser()
        {
            var userProfile = GetUserProfile();
            return userProfile.IsNull();
        }

        public string Name()
        {
            var userProfile = GetUserProfile();
            if (userProfile.IsNull())
            {
                return null;
            }
            return userProfile.Name;
        }

        private UserProfile GetUserProfile()
        {
            var email = _httpContextAccessor.HttpContext.Request.Cookies["email"];
            if (email.IsNull())
            {
                return null;
            }

            return _userService.GetUserProfileByEmailAsync(email).GetAwaiter().GetResult();
        }
    }
}
