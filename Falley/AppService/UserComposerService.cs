﻿using AutoMapper;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.ViewModel.AppSettings;
using Falley.ViewModel.Duties;
using Falley.ViewModel.Experiences;
using Falley.ViewModel.Users;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Falley.AppService
{
    public class UserComposerService : IUserComposerService
    {

        private readonly IExperienceService _experienceService;
        private readonly IDutyService _dutyService;
        private readonly IPhotoService _photoService;
        private readonly IProjectService _projectService;
        private readonly IResumeService _resumeService;
        private readonly IEducationService _educationService;
        private readonly ISkillService _skillService;
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;

        public UserComposerService(
            IExperienceService experienceService,
            IDutyService dutyService,
            IPhotoService photoService,
            IProjectService projectService,
            IResumeService resumeService,
            IEducationService educationService,
            ISkillService skillService,
            IUserService userService,
            IConfiguration configuration)
        {
            _experienceService = experienceService;
            _dutyService = dutyService;
            _photoService = photoService;
            _projectService = projectService;
            _resumeService = resumeService;
            _educationService = educationService;
            _skillService = skillService;
            _userService = userService;
            _configuration = configuration;
        }

        public async Task<IList<ExperienceDutyViewModel>> GetExperienceDuties(Guid userId)
        {
            var experienceProfiles = await _experienceService.GetExperienceProfilesAsync(userId);
            var experienceDuties = new List<ExperienceDutyViewModel>();

            foreach (var experienceProfile in experienceProfiles)
            {
                var duties = await _dutyService.GetDutyProfilesAsync(experienceProfile.Id);
                experienceDuties.Add(new ExperienceDutyViewModel
                {
                    DutyProfiles = Mapper.Map<List<SaveDutiesViewModel>>(duties),
                    ExperienceProfile = Mapper.Map<SaveExperienceViewModel>(experienceProfile)
                });
            }

            return experienceDuties;
        }

        public async Task<UserViewModel> GetUserDetailsAsync(Guid userId)
        {
            var userViewModel = new UserViewModel();

            var userProfile = await _userService.GetUserProfileAsync(userId);
            var photoProfile = await _photoService.GetPhotoProfileAsync(userId);
            var resumeProfile = await _resumeService.GetResumeProfileAsync(userId);
            var skillProfile = await _skillService.GetSkillProfilesAsync(userId);
            var educationProfile = await _educationService.GetEducationProfilesAsync(userId);
            var projectProfile = await _projectService.GetProjectProfiles(userId);
            var experienceProfiles = await _experienceService.GetExperienceProfilesAsync(userId);

            userViewModel.ExperienceDutiesViewModel = new List<ExperienceDutyViewModel>();

            if (!experienceProfiles.IsNullOrEmpty())
            {
                foreach (var experience in experienceProfiles)
                {
                    var dutyProfile = Mapper.Map<IList<SaveDutiesViewModel>>(await _dutyService.GetDutyProfilesAsync(experience.Id));
                    userViewModel.ExperienceDutiesViewModel.Add(new ExperienceDutyViewModel
                    {
                        ExperienceProfile = Mapper.Map<SaveExperienceViewModel>(experience),
                        DutyProfiles = dutyProfile
                    });
                }
            }

            userViewModel.ProjectProfiles = projectProfile;
            userViewModel.Resume = resumeProfile;
            userViewModel.SkillProfiles = skillProfile;
            userViewModel.EducationProfiles = educationProfile;
            userViewModel.UserProfile = userProfile;
            userViewModel.AnyPhoto = !photoProfile.IsNull();

            return userViewModel;
        }

        //public void HandleSetCookieSession(string userName, string userId, int? expireTime)
        //{
        //    CookieOptions option = new CookieOptions();

        //    if (expireTime.HasValue)
        //    {
        //        option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
        //    }
        //    else
        //    {
        //        option.Expires = DateTime.Now.AddMilliseconds(10);
        //    }

        //    var keyValuePairUserName = new KeyValuePair<string, string>(nameof(userName), userName);
        //    var keyValuePairId = new KeyValuePair<string, string>(nameof(userId), userId);

        //    _httpContextAccessor.HttpContext.Request.Cookies.Append(keyValuePairUserName);
        //    _httpContextAccessor.HttpContext.Request.Cookies.Append(keyValuePairId);

        //}

        //public void RemoveCookieSession()
        //{
        //    _httpContextAccessor.HttpContext.Response.Cookies.Delete("userName");
        //    _httpContextAccessor.HttpContext.Response.Cookies.Delete("userId");
        //}

        public string HandleToken(Guid userId)
        {
            var appSettingConfigurationModel = _configuration.GetSection("AppSettings")
                .Get<AppSettingsConfigurationModel>();

            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(appSettingConfigurationModel.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userId.ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);

        }
    }

    public interface IUserComposerService
    {
        Task<UserViewModel> GetUserDetailsAsync(Guid id);

        string HandleToken(Guid userId);

        Task<IList<ExperienceDutyViewModel>> GetExperienceDuties(Guid userId);

        //void HandleSetCookieSession(string userName, string userId, int? expireTime);

        //void RemoveCookieSession();
    }
}
