﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Educations;
using Microsoft.AspNetCore.Mvc;

namespace Falley.Controllers
{
    public class EducationsController : Controller
    {
        private readonly IEducationService _educationService;
        private readonly IUserContext _userContext;
        public EducationsController(
            IEducationService educationService, 
            IUserContext userContext)
        {
            _educationService = educationService;
            _userContext = userContext;
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SaveEducationViewModel model)
        {
            var educationProfile = Mapper.Map<EducationProfile>(model);
            var userId = _userContext.UserId();
            var educationId = await _educationService.CreateEducationAsync(educationProfile, userId);

            return RedirectToAction("Details", new { id = userId });
        }

        public async Task<IActionResult> Details(Guid userId)
        {
            var educationProfiles = await _educationService.GetEducationProfilesAsync(userId);
            if (educationProfiles.IsNullOrEmpty())
            {
                return RedirectToAction("Create");
            }
            var profiles = Mapper.Map<List<SaveEducationViewModel>>(educationProfiles);

            return View(profiles);
        }
        

        public async Task<IActionResult> Edit(Guid id)
        {
            var education  = await _educationService.GetEducationProfileAsync(id);

            var educationViewModel = Mapper.Map<SaveEducationViewModel>(education);

            return View(educationViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SaveEducationViewModel model)
        {
            var userId = _userContext.UserId();

            var educationProfile = Mapper.Map<EducationProfile>(model);

            await _educationService.UpdateEducationAsync(educationProfile, userId);

            return RedirectToAction("Details", "Educations", new { userId });
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var userId = _userContext.UserId();

            await _educationService.DeleteEducationAsync(id);

            return RedirectToAction("Details", new { id = userId });
        }
    }
}