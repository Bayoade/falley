﻿using AutoMapper;
using Falley.AppService;
using Falley.Bootstraps;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Account;
using Falley.ViewModel.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Falley.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IUserComposerService _userComposerService;
        private readonly IUserService _userService;

        public AccountController (
                IAccountService accountService,
                IUserComposerService userComposerService,
                IUserService userService
            )
        {
            _accountService = accountService;
            _userComposerService = userComposerService;
            _userService = userService;
        }
        
        public IActionResult Login( string response = null)
        {
            RemoveCookieSession();

            ViewBag.Response = response;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            var user = await _accountService.AuthenticateAsync(loginViewModel.Email, loginViewModel.Password);

            if (user == null)
            {
                var response = "Username or password is incorrect";
                return RedirectToAction("Login", new { response });
            }

            HandleSetCookieSession(user.Email, 7);

            var url = String.Format("/users/{0}/details", user.Id );

            return Redirect(url);
        }

        [Route("account/register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [Route("account/register")]
        public async Task<IActionResult> Register(SaveUserViewModel model)
        {
            var user = Mapper.Map<UserProfile>(model);

            try
            {
                await _accountService.CreateUserProfileAsync(user);
                return Ok();
            }

            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [Route("account/{id:guid}/update")]
        public async Task<IActionResult> UpdateUser(Guid id, UserUpdateViewModel model)
        {
            var user = Mapper.Map<UserProfile>(model);
            user.Id = id;

            try
            {
                await _accountService.UpdateUserProfileDetailsAsync(user);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [Route("account/{id:guid}/delete")]
        public async Task<IActionResult> DeleteUser(Guid userId)
        {
            await _userService.DeleteUserAsync(userId);
            return Ok();
        }

        [Route("account/{userId:guid}/loginupdates")]
        public async Task<IActionResult> LoginUpdate(Guid userId, LoginViewModel loginModel)
        {
            await _accountService.UpdatePasswordLoginAsync(Mapper.Map<UserProfile>(loginModel));
            return Ok();
        }

        public IActionResult LogOut()
        {
            RemoveCookieSession();
            return RedirectToAction("Login");
        }

        private void HandleSetCookieSession(string email, int? expireTime)
        {
            CookieOptions option = new CookieOptions();

            if (expireTime.HasValue)
            {
                option.Expires = DateTime.Now.AddDays(expireTime.Value);
            }
            else
            {
                option.Expires = DateTime.Now.AddMilliseconds(10);
            }
            
            Response.Cookies.Append(nameof(email), email, option);
        }

        private void RemoveCookieSession()
        {
            foreach (var cookie in Request.Cookies.Keys)
            {
                Response.Cookies.Delete(cookie);
                Response.Redirect("/");
            }
            //var cookie = Request.Cookies[IoCContainer.Configuration["Cookie:Key"]];
            //if (!cookie.IsNull())
            //{
            //    Response.Cookies.Delete(cookie);
            //    Response.Redirect("/");
            //}
        }
    }
}