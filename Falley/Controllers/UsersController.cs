﻿using AutoMapper;
using Falley.AppService;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.Helpers;
using Falley.ViewModel.Users;
using Microsoft.AspNetCore.Mvc;
using MimeMapping;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Falley.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserComposerService _userComposerService;
        private readonly IPhotoService _photoService;
        private readonly IUserService _userService;
        //private readonly IViewHelper _viewHelper;
        private readonly IAccountService _accountService;

        public UsersController(
            IUserComposerService userComposerService,
            IPhotoService photoService,
            IUserService userService,
            //IViewHelper viewHelper,
            IAccountService accountService
            )
        {
            _userComposerService = userComposerService;
            _photoService = photoService;
            _userService = userService;
            //_viewHelper = viewHelper;
            _accountService = accountService;
        }

        /// <summary>
        /// get profile details of a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IActionResult> Profile(Guid userId)
        {
            var userViewModel = await _userComposerService.GetUserDetailsAsync(userId);

            return View(userViewModel);
        }

        [Route("users/profiles")]
        public async Task<IActionResult> GetAllUserProfiles()
        {
            var userProfiles = await _userService.GetAllUserProiles();
            var userViewModels = Mapper.Map<List<UserDetailsUpdateViewModel>>(userProfiles);

            return Ok(userViewModels);
        }

        [HttpGet("users/{userId:guid}/details")]
        public async Task<IActionResult> Details(Guid userId)
        {
            var userProfile = await _userService.GetUserProfileAsync(userId);
            var user = Mapper.Map<UserDetailsUpdateViewModel>(userProfile);
            //var siteModel = new SiteViewHelperModel
            //{
            //    UserId = id,
            //    ViewHelper = _viewHelper
            //};

            return View(user);
        }

        [Route("users/{id:guid}/edit")]
        public async Task<IActionResult> Edit(Guid id)
        {
            var userProfile = await _userService.GetUserProfileAsync(id);
            var user = Mapper.Map<UserDetailsUpdateViewModel>(userProfile);

            return View(user);
        }

        [HttpPost("users/{id:guid}/edit")]
        public async Task<IActionResult> Edit(UserDetailsUpdateViewModel model)
        {
            var userProfile = Mapper.Map<UserProfile>(model);
            await _accountService.UpdateUserProfileDetailsAsync(userProfile);
            
            return RedirectToAction("Details", new { id = model.Id });
        }
    }
}