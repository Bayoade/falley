﻿using AutoMapper;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Resumes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Falley.Controllers
{
    public class ResumesController : Controller
    {
        private readonly IResumeService _resumeService;
        private readonly IUserContext _userContext;
        public ResumesController(IResumeService resumeService, IUserContext userContext)
        {
            _resumeService = resumeService;
            _userContext = userContext;
        }
        [Route("users/{id:guid}/resume")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost("users/{id:guid}/resume")]
        public async Task<IActionResult> Create(SaveResumeViewModel model)
        {
            var userId = _userContext.UserId();
            var resumeProfile = Mapper.Map<ResumeProfile>(model);
            
            var resumeId = await _resumeService.CreateResumeAsync(resumeProfile, userId);

            var url = String.Format("/users/{0}/resume/details", userId);

            return Redirect(url);
        }

        [HttpGet("users/{userId:guid}/resume/details")]
        public async Task<IActionResult> Details(Guid userId)
        {
            var model = await _resumeService.GetResumeProfileAsync(userId);

            if (model.IsNull())
            {
                return RedirectToAction("Create");
            }

            var resumeProfile = Mapper.Map<SaveResumeViewModel>(model);

            return View(resumeProfile);
        }

        public async Task<IActionResult> Edit(Guid userId)
        {
            var resume = await _resumeService.GetResumeProfileAsync(userId);
            var resumeView = Mapper.Map<SaveResumeViewModel>(resume);
            return View(resumeView);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SaveResumeViewModel model)
        {
            var userId = _userContext.UserId();

            var resumeProfile = Mapper.Map<ResumeProfile>(model);

            await _resumeService.UpdateResumeAsync(resumeProfile, userId);

            return RedirectToAction("Details", new { userId });
        }

        public async Task<IActionResult> Delete(Guid userId)
        {
            await _resumeService.DeleteResumeAsync(userId);
            var url = String.Format("/users/{0}/resume/details", userId);

            return Redirect(url);
        }
    }
}