﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Skills;
using Microsoft.AspNetCore.Mvc;

namespace Falley.Controllers
{
    public class SkillsController : Controller
    {
        private readonly ISkillService _skillService;
        private readonly IUserContext _userContext;
        public SkillsController(ISkillService skillService, IUserContext userContext)
        {
            _skillService = skillService;
            _userContext = userContext;
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SaveSkillViewModel model)
        {
            var userId = _userContext.UserId();
            var skillProfile = Mapper.Map<SkillProfile>(model);
            var skillId = await _skillService.CreateSkillProfileAsync(skillProfile, userId);

            return RedirectToAction("Details", new { id = userId });
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var userId = _userContext.UserId();
            var skills = await _skillService.GetSkillProfilesAsync(userId);

            if (skills.IsNullOrEmpty())
            {
                return RedirectToAction("Create");
            }

            var skillProfiles = Mapper.Map<List<SaveSkillViewModel>>(skills);

            return View(skillProfiles);
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var userId = _userContext.UserId();
            await _skillService.DeleteSkillProfileAsync(id);

            return RedirectToAction("Details", new { id = userId });
        }
    }
}