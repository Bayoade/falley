﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Projects;
using Microsoft.AspNetCore.Mvc;

namespace Falley.Controllers
{
    public class ProjectsController : Controller
    {
        private readonly IProjectService _projectService;
        private readonly IUserContext _userContext;
        public ProjectsController(IProjectService projectService, IUserContext userContext)
        {
            _projectService = projectService;
            _userContext = userContext;
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SaveProjectViewModel model)
        {
            var projectProfile = Mapper.Map<ProjectProfile>(model);
            var userId = _userContext.UserId();
            await _projectService.CreateProjectAsync(projectProfile, userId);

            return RedirectToAction("Details", new { id = userId });
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var userId = _userContext.UserId();

            var projects = await _projectService.GetProjectProfiles(userId);
            if (projects.IsNullOrEmpty())
            {
                return RedirectToAction("Create");
            }
            var projectViewModel = Mapper.Map<List<SaveProjectViewModel>>(projects);

            return View(projectViewModel);
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var userId = _userContext.UserId();
            await _projectService.DeleteProjectAsync(id);

            return RedirectToAction("Details", new { id = userId });
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var project = await _projectService.GetProjectProfile(id);

            var projectViewModel = Mapper.Map<SaveProjectViewModel>(project);

            return View(projectViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SaveProjectViewModel model)
        {
            var userId = _userContext.UserId();
            var projectProfile = Mapper.Map<ProjectProfile>(model);
            await _projectService.UpdateProjectAsync(projectProfile, userId);

            return RedirectToAction("Details", new { id = userId });
        }
    }
}