﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Falley.AppService;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Experiences;
using Microsoft.AspNetCore.Mvc;

namespace Falley.Controllers
{
    public class ExperiencesController : Controller
    {
        private readonly IExperienceService _experienceService;
        private readonly IUserComposerService _userComposerService;
        private readonly IUserContext _userContext;
        public ExperiencesController(
            IExperienceService experienceService,
            IUserComposerService userComposerService,
            IUserContext userContext)
        {
            _experienceService = experienceService;
            _userComposerService = userComposerService;
            _userContext = userContext;
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SaveExperienceViewModel model)
        {
            var userId = _userContext.UserId();
            var profile = Mapper.Map<ExperienceProfile>(model);
            await _experienceService.CreateExperienceAsync(profile, userId);
            return RedirectToAction("Details", new { userId });
        }

        public async Task<IActionResult> Details(Guid userId)
        {
            var experiences = await _userComposerService.GetExperienceDuties(userId);
            if (experiences.IsNullOrEmpty())
            {
                return RedirectToAction("Create");
            }

            return View(Mapper.Map<List<ExperienceDutyViewModel>>(experiences));
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var experience = await _experienceService.GetExperienceProfileAsync(id);
            var experienceSaveViewModel = Mapper.Map<SaveExperienceViewModel>(experience);
            return View(experienceSaveViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SaveExperienceViewModel model)
        {
            var userId = _userContext.UserId();
            var experienceprofile = Mapper.Map<ExperienceProfile>(model);

            await _experienceService.UpdateExperienceAsync(experienceprofile, userId);

            return RedirectToAction("Details", new { userId });
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var userId = _userContext.UserId();
            await _experienceService.DeleteExperienceAsync(id);

            return RedirectToAction("Details", new { userId });
        }
    }
}