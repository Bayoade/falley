﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Falley.AppService;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Photos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeMapping;

namespace Falley.Controllers
{
    public class PhotosController : Controller
    {
        private readonly IUserContext _userContext;
        private readonly IPhotoService _photoService;

        public PhotosController(IPhotoService photoService, IUserContext userContext)
        {
            _photoService = photoService;
            _userContext = userContext;
        }

        public IActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(List<IFormFile> files)
        {
            var userId = _userContext.UserId();

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    var photo = new PhotoProfile();
                    using (Image img = Image.FromStream(formFile.OpenReadStream()))
                    {
                        photo.FileData = img.Resize(150, 150).ToByteArray();
                    }

                    photo.FileName = formFile.FileName;

                    await _photoService.CreatePhotoAsync(photo, userId);
                }
            }

            return RedirectToAction("Details");
        }

        public async Task<IActionResult> Details()
        {
            var userId = _userContext.UserId();
            var anyPhoto = true;
            var photo = await _photoService.GetPhotoProfileAsync(userId);

            if (photo.IsNull())
            {
                anyPhoto = false;
            }

            return View(new PhotoDisplayViewModel
            {
                AnyPhoto = anyPhoto,
                UserId = userId
            });
        }

        public async Task<IActionResult> Download()
        {
            var userId = _userContext.UserId();
            var photoProfile = await _photoService.GetPhotoProfileAsync(userId);
            if (photoProfile.IsNull())
            {
                return null;
            }

            var outputStream = new MemoryStream(photoProfile.FileData);
            outputStream.Seek(0, SeekOrigin.Begin);

            return new FileStreamResult(outputStream, MimeUtility.GetMimeMapping(photoProfile.FileName));
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            await _photoService.DeletePhotoAsync(id);
            return RedirectToAction("Details");
        }
    }
}