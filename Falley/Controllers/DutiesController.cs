﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Falley.Core.Extensions;
using Falley.Core.IService;
using Falley.Core.Model;
using Falley.ViewModel.Duties;
using Microsoft.AspNetCore.Mvc;

namespace Falley.Controllers
{
    public class DutiesController : Controller
    {
        private readonly IDutyService _dutyService;
        private readonly IUserContext _userContext;
        public DutiesController(
            IDutyService dutyService, 
            IUserContext userContext)
        {
            _dutyService = dutyService;
            _userContext = userContext;
        }
        public IActionResult Create(Guid experienceId)
        {
            ViewBag.ExperienceId = experienceId;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SaveDutiesViewModel model, Guid experienceId)
        {
            var userId = _userContext.UserId();

            var dutyProfile = Mapper.Map<DutyProfile>(model);

            await _dutyService.CreateDutyAsync(dutyProfile, experienceId);

            return RedirectToAction("Details", "Experiences", new { userId });
        }

        public async Task<IActionResult> Details(Guid experienceId)
        {
            var profiles = await _dutyService.GetDutyProfilesAsync(experienceId);

            if (profiles.IsNullOrEmpty())
            {
                return RedirectToAction("Create", new { experienceId });
            }

            return View(Mapper.Map<List<DutyProfile>>(profiles));
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            await _dutyService.DeleteDutyAsync(id);
            var userId = _userContext.UserId();

            return RedirectToAction("Details", "Experiences", new { userId });
        }
    }
}