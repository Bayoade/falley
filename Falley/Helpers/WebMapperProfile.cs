﻿using AutoMapper;
using Falley.Core.Model;
using Falley.ViewModel;
using Falley.ViewModel.Account;
using Falley.ViewModel.Duties;
using Falley.ViewModel.Educations;
using Falley.ViewModel.Experiences;
using Falley.ViewModel.Projects;
using Falley.ViewModel.Skills;
using Falley.ViewModel.Users;

namespace Falley.Helpers
{
    public class WebMapperProfile : Profile
    {
        public WebMapperProfile()
        {
            CreateMap<SaveUserViewModel, UserProfile>()
                .ForMember(x => x.Salt, y => y.Ignore());

            CreateMap<UserUpdateViewModel, UserProfile>()
                .ForMember(x => x.Name, y => y.Ignore())
                .ForMember(x => x.Password, y => y.Ignore())
                .ForMember(x => x.Salt, y => y.Ignore())
                .ReverseMap();

            CreateMap<UserDetailsUpdateViewModel, UserProfile>()
                .ForMember(x => x.Name, y => y.Ignore())
                .ForMember(x => x.Password, y => y.Ignore())
                .ForMember(x => x.Salt, y => y.Ignore())
                .ReverseMap();

            CreateMap<LoginViewModel, UserProfile>()
                .ForMember(x => x.Id, y => y.Ignore())
                .ForMember(x => x.Name, y => y.Ignore())
                .ForMember(x => x.FirstName, y => y.Ignore())
                .ForMember(x => x.MiddleName, y => y.Ignore())
                .ForMember(x => x.LastName, y => y.Ignore())
                .ForMember(x => x.Salt, y => y.Ignore());

            CreateMap<SaveEducationViewModel, EducationProfile>()
                .ReverseMap();

            CreateMap<SaveProjectViewModel, ProjectProfile>()
                .ReverseMap();

            CreateMap<SaveSkillViewModel, SkillProfile>()
                .ReverseMap();

            CreateMap<SaveExperienceViewModel, ExperienceProfile>()
                .ReverseMap();

            CreateMap<SaveDutiesViewModel, DutyProfile>()
                .ReverseMap();
        }
    }
}
