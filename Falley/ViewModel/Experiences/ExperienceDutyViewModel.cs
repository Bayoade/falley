﻿using Falley.Core.Model;
using Falley.ViewModel.Duties;
using System.Collections.Generic;

namespace Falley.ViewModel.Experiences
{
    public class ExperienceDutyViewModel
    {
        public SaveExperienceViewModel ExperienceProfile { get; set; }

        public IList<SaveDutiesViewModel> DutyProfiles { get; set; }
    }
}
