﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Falley.ViewModel.Experiences
{
    public class SaveExperienceViewModel
    {
        public Guid Id { get; set; }

        public string Company { get; set; }

        public string Position { get; set; }

        public string Period { get; set; }

        public string Website { get; set; }
    }
}
