﻿using System;

namespace Falley.ViewModel.Resumes
{
    public class SaveResumeViewModel
    {
        public Guid Id { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Position { get; set; }

        public string Email { get; set; }

        public string Description { get; set; }
    }
}
