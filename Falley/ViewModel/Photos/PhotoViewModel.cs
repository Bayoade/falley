﻿using Microsoft.AspNetCore.Mvc;

namespace Falley.ViewModel.Photos
{
    public class PhotoViewModel
    {
        public FileStreamResult File { get; set; }
    }
}
