﻿using Microsoft.AspNetCore.Http;

namespace Falley.ViewModel.Photos
{
    public class SavePhotoViewModel
    {
        public IFormFile FormFile { get; set; }
    }
}
