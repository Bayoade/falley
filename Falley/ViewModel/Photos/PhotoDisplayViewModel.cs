﻿using System;

namespace Falley.ViewModel.Photos
{
    public class PhotoDisplayViewModel
    {
        public bool AnyPhoto { get; set; }

        public Guid UserId { get; set; }
    }
}
