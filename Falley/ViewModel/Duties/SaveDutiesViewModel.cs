﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Falley.ViewModel.Duties
{
    public class SaveDutiesViewModel
    {
        public Guid Id { get; set; }

        public string Workdone { get; set; }
    }
}
