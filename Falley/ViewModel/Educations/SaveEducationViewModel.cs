﻿using System;

namespace Falley.ViewModel.Educations
{
    public class SaveEducationViewModel
    {
        public Guid Id { get; set; }

        public string School { get; set; }

        public string Degree { get; set; }

        public string Period { get; set; }
    }
}
