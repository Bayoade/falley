﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Falley.ViewModel.Account;

namespace Falley.ViewModel.Users
{
    public class SaveUserViewModel : PasswordViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Email { get; set; }
    }
}