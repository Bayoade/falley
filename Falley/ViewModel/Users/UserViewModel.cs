﻿using Falley.Core.Model;
using Falley.ViewModel.Experiences;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;

namespace Falley.ViewModel.Users
{
    public class UserViewModel
    {
        public UserProfile UserProfile { get; set; }

        public bool AnyPhoto { get; set; }

        public IList<ExperienceDutyViewModel> ExperienceDutiesViewModel { get; set; }

        public ResumeProfile Resume { get; set; }

        public IList<EducationProfile> EducationProfiles { get; set; }

        public IList<SkillProfile> SkillProfiles { get; set; }

        public IList<ProjectProfile> ProjectProfiles { get; set; }

    }
}
