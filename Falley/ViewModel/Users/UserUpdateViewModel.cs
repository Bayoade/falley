﻿namespace Falley.ViewModel.Users
{
    public class UserUpdateViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string UserName { get; set; }
    }
}
