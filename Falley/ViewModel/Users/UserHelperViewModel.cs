﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Falley.ViewModel.Users
{
    public class UserHelperViewModel
    {
        public string UserId { get; set; }

        public string  UserName { get; set; }
    }
}
