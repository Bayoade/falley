﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Falley.ViewModel.Skills
{
    public class SaveSkillViewModel
    {
        public Guid Id { get; set; }

        public string SkillName { get; set; }
    }
}
