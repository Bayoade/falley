﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Falley.ViewModel.AppSettings
{
    public class AppSettingsConfigurationModel
    {
        public string Secret { get; set; }
    }
}
