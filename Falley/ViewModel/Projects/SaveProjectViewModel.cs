﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Falley.ViewModel.Projects
{
    public class SaveProjectViewModel
    {
        public Guid Id { get; set; }

        public string ProjectRepository { get; set; }

        public string ProjectName { get; set; }
    }
}
