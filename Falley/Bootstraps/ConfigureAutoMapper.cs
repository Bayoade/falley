﻿using AutoMapper;
using Falley.Data.Sql;
using Falley.Helpers;
using Falley.Service;
using Microsoft.Extensions.DependencyInjection;

namespace Falley.Bootstraps
{
    public static class ConfigureAutoMapper
    {
        public static void AddAutoMappers(this IServiceCollection services)
        {
            Mapper.Initialize(cfg =>
                   {
                       cfg.AddProfile<SqlMapperProfile>();
                       cfg.AddProfile<WebMapperProfile>();
                       cfg.AddProfile<ServiceMapperProfile>();
                   }
            );
        }
    }
}
