﻿using Falley.Data.Sql.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Falley.Bootstraps
{
    public static class ConfigureDbContext
    {
        public static void AddDbContextConfiguration(this IServiceCollection services)
        {
            var connectionString = IoCContainer.Configuration.GetConnectionString("FalleyConnection.Key");
            
            services.AddEntityFrameworkNpgsql().AddDbContext<FalleyDbContext>(
                dbContextOptionBulder =>
                {
                    dbContextOptionBulder.UseNpgsql(connectionString);
                });
        }
    }
}
