﻿//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.IdentityModel.Tokens;
//using System;
//using System.Text;

//namespace Falley.Bootstraps
//{
//    public static class ConfigureJwtAuthentication
//    {
//        public static void AddJwtAuthentication(this IServiceCollection services)
//        {
//            services.AddAuthentication()
//                .AddJwtBearer(options =>
//                {
//                    options.TokenValidationParameters = new TokenValidationParameters
//                    {
//                        ClockSkew = TimeSpan.FromMinutes(5),
//                        RequireSignedTokens = true,
//                        ValidateLifetime = true,
//                        RequireExpirationTime = true,
//                        ValidateIssuer = true,
//                        ValidateAudience = true,
//                        ValidateIssuerSigningKey = true,
//                        ValidIssuer = IoCContainer.Configuration["Jwt:JwtIssuer"],
//                        ValidAudience = IoCContainer.Configuration["Jwt:JwtAudience"],
//                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(IoCContainer.Configuration["JWT:SecretKey"]))
//                    };
//                });
//        }
//    }
//}
