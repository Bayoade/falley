﻿using Falley.AppService;
using Falley.Core.IRepository;
using Falley.Core.IService;
using Falley.Data.Sql.Repository;
using Falley.Helpers;
using Falley.Service;
using Falley.Service.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Falley.Bootstraps
{
    public static class ConfigureDependencies
    {
        public static void AddDepedencies(this IServiceCollection services)
        {

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

            /// Add Repositories
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPhotoRepository, PhotoRepository>();
            services.AddScoped<IDutyRepository, DutyRepository>();
            services.AddScoped<IResumeRepository, ResumeRepository>();
            services.AddScoped<IExperienceRepository, ExperienceRepository>();
            services.AddScoped<IEducationRepository, EducationRepository>();
            services.AddScoped<ISkillRepository, SkillRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();


            /// Add Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPhotoService, PhotoService>();
            services.AddScoped<IDutyService, DutyService>();
            services.AddScoped<IResumeService, ResumeService>();
            services.AddScoped<IExperienceService, ExperienceService>();
            services.AddScoped<IEducationService, EducationService>();
            services.AddScoped<ISkillService, SkillService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IUserComposerService, UserComposerService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUserContext, UserContext>();
            //services.AddScoped<IViewHelper, ViewHelper>();
        }
    }
}
