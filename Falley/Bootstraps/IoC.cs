﻿using Falley.Data.Sql.Context;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Falley.Bootstraps
{
    public static class IoC
    {
        public static FalleyDbContext FalleyDbContext => IoCContainer
            .Provider
            .GetService<FalleyDbContext>();
    }

    public static class IoCContainer
    {
        public static IServiceProvider Provider { get; set; }

        public static IConfiguration Configuration { get; set; }
    }
}
