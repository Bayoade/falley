﻿using Falley.Bootstraps;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Faylley
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            IoCContainer.Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextConfiguration();

            services.AddAutoMappers();

            //services.AddJwtAuthentication();

            services.AddDepedencies();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            //services.AddAuthenticationConfiguration(Configuration);

            //services.AddMvc(options =>
            //{
            //    options.Filters.Add<ModelValidationFilter>();
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseCors(x => x
               .AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader()
               );

            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //    name: "moviesByReleaseDate",
                //    template: "Movies/Released/{year}/{month}",
                //    //constraints: new { year = @"\d{4}", month = @"\d{2}" },
                //    constraints: new { year = @"2015 | 2016", month = @"\d{2}" },
                //    defaults: new {controller = nameof(MoviesController), action= nameof(MoviesController.ByReleaseDate) });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
